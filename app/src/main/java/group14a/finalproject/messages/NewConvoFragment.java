package group14a.finalproject.messages;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import group14a.finalproject.R;

/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class NewConvoFragment extends Fragment {

    private static final String TAG = "NewConvoFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_new_convo, container, false);

        Button sendMessage = ButterKnife.findById(view, R.id.send_message);
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView usernameView = ButterKnife.findById(view, R.id.edit_username);
                final String username = usernameView.getText().toString();
                TextView messageView = ButterKnife.findById(view, R.id.edit_message_text);
                final String messageText = messageView.getText().toString();

                // Find user by username
                ParseQuery<ParseUser> usersQuery = ParseUser.getQuery();
                usersQuery.whereEqualTo("username", username);
                usersQuery.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> parseUsers, ParseException e) {
                        if (e == null) {
                            // Make message readable to both users
                            ParseUser sender = ParseUser.getCurrentUser();
                            final ParseUser recipient = parseUsers.get(0);
                            ParseACL acl = new ParseACL();
                            acl.setReadAccess(sender, true);
                            acl.setReadAccess(recipient, true);
                            acl.setWriteAccess(recipient, true);

                            // Create new message
                            ParseObject message = new ParseObject("Message");
                            message.setACL(acl);
                            message.put("sender", sender);
                            message.put("recipient", recipient);
                            message.put("text", messageText);
                            message.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        String otherUserId = recipient.getObjectId();
                                        String otherUserName = recipient.getString("firstName") + " " + recipient.getString("lastName");

                                        getActivity().getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.fragment_container, ConvoFragment.newInstance(otherUserId, otherUserName))
                                                .commit();
                                        onDestroy();
                                    } else {
                                        Log.e(TAG, e.getMessage());
                                    }
                                }
                            });
                        } else {
                            Log.e(TAG, e.getMessage());
                            Toast.makeText(getContext(), "User not found", Toast.LENGTH_SHORT);
                        }
                    }
                });

            }
        });

        return view;
    }

}
