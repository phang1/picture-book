package group14a.finalproject.messages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import group14a.finalproject.R;
import group14a.finalproject.UsersListActivity;
import group14a.finalproject.UsersListAdapter;
import group14a.finalproject.profile.ProfileFragment;

/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class ConvosListFragment extends ListFragment {

    private static final String TAG = "ConvosListFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Messages");

        setEmptyText(getString(R.string.no_messages_found));

        // Get messages from inbox; Should only return accessible messages
        final ParseQuery<ParseObject> messagesQuery = ParseQuery.getQuery("Message");
        messagesQuery.orderByDescending("updatedAt");
        messagesQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> messages, ParseException e) {
                if (e == null) {
                    Log.d(TAG, "Retrieved " + messages.size() + " message");

                    // Create list of other users from messages
                    LinkedHashSet<ParseUser> otherUsersSet = new LinkedHashSet<ParseUser>();
                    for (ParseObject message : messages) {
                        otherUsersSet.add(message.getParseUser("sender"));
                        otherUsersSet.add(message.getParseUser("recipient"));
                    }
                    otherUsersSet.remove(ParseUser.getCurrentUser());
                    ArrayList<ParseUser> otherUsersList = new ArrayList<ParseUser>(otherUsersSet);
                    Log.d(TAG, "Found " + otherUsersList.size() + " users");

                    setListAdapter(new UsersListAdapter(getContext(), otherUsersList));
                } else {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_convos_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.goto_new_convo:
                // Show list of users
                Intent intent = new Intent(getContext(), UsersListActivity.class);
                startActivityForResult(intent, UsersListActivity.PICK_USER);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.d(TAG, "clicked user " + position);

        ParseUser user = (ParseUser) getListView().getItemAtPosition(position);
        String userId = user.getObjectId();
        String userName = user.getString("firstName") + " " + user.getString("lastName");

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, ConvoFragment.newInstance(userId, userName))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == UsersListActivity.PICK_USER) {
                Log.i(TAG, "picked user");
                String userId = data.getStringExtra("userId");
                String userName = data.getStringExtra("userName");
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, ConvoFragment.newInstance(userId, userName))
                        .addToBackStack(null)
                        .commit();
            }
        }
    }
}
