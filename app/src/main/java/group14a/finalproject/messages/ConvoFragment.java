package group14a.finalproject.messages;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import group14a.finalproject.R;

public class ConvoFragment extends Fragment {

    private static final String TAG = "ConvoFragment";

    // Intent request code
    private static final int SELECT_IMAGE = 1001;

    private static final String OTHER_USER_ID = "otherUserId";
    private static final String OTHER_USER_NAME = "otherUserName";

    private String otherUserId;
    private String otherUserName;
    private ParseUser otherUser;

    private ListView listView;
    private List<ParseObject> mMessages;

    public static ConvoFragment newInstance(String otherUserId, String otherUserName) {
        ConvoFragment fragment = new ConvoFragment();
        Bundle args = new Bundle();
        args.putString(OTHER_USER_ID, otherUserId);
        args.putString(OTHER_USER_NAME, otherUserName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            otherUserId = getArguments().getString(OTHER_USER_ID);
            otherUserName = getArguments().getString(OTHER_USER_NAME);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_convo, container, false);
        getActivity().setTitle(otherUserName);

        listView = ButterKnife.findById(view, R.id.list);

        TextView emptyListView = ButterKnife.findById(view, R.id.empty_list);
        listView.setEmptyView(emptyListView);

        otherUser = ParseUser.createWithoutData(ParseUser.class, otherUserId);

        // Get messages with other user
        queryAndShowMessages();

        // Create send message button listener
        Button sendMessage = ButterKnife.findById(view, R.id.send_message);
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView messageView = ButterKnife.findById(view, R.id.edit_message_text);
                final String messageText = messageView.getText().toString();

                // Make message readable to both users
                ParseUser sender = ParseUser.getCurrentUser();
                ParseUser recipient = otherUser;
                ParseACL acl = new ParseACL();
                acl.setReadAccess(sender, true);
                acl.setReadAccess(recipient, true);
                acl.setWriteAccess(recipient, true);

                // Create new message
                final ParseObject message = new ParseObject("Message");
                message.setACL(acl);
                message.put("sender", sender);
                message.put("recipient", recipient);
                message.put("text", messageText);
                message.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            messageView.setText("");

                            // Refresh list view with new message
                            mMessages.add(message);
                            listView.setAdapter(new MessagesListAdapter(getContext(), mMessages));
                        } else {
                            Toast.makeText(getContext(), "Failed to send message", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
            }
        });

        return view;
    }

    private void queryAndShowMessages() {
        // Get messages with other user
        final ParseQuery<ParseObject> messagesQuery = ParseQuery.or(Arrays.asList(
                ParseQuery.getQuery("Message").whereEqualTo("sender", otherUser),
                ParseQuery.getQuery("Message").whereEqualTo("recipient", otherUser)
        ));
        messagesQuery.orderByAscending("updateAt");
        messagesQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> messages, ParseException e) {
                if (e == null) {
                    mMessages = messages;
                    listView.setAdapter(new MessagesListAdapter(getContext(), mMessages));
                } else {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_convo, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send_image:
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, SELECT_IMAGE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE) {
                final Uri imageUri = data.getData();
                try {
                    final ProgressDialog progressDialog = ProgressDialog.show(getContext(),
                            getString(R.string.loading), "", true);

                    // URI -> bitmap -> PNG byte[] -> ParseFile
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(
                            getContext().getContentResolver(), imageUri);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    final ParseFile msgImage = new ParseFile("image.png", stream.toByteArray());

                    // Upload image to Parse
                    progressDialog.setIndeterminate(false);
                    progressDialog.setMax(100);
                    msgImage.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                // Make message readable to both users
                                ParseUser sender = ParseUser.getCurrentUser();
                                ParseUser recipient = otherUser;
                                ParseACL acl = new ParseACL();
                                acl.setReadAccess(sender, true);
                                acl.setReadAccess(recipient, true);
                                acl.setWriteAccess(recipient, true);

                                // Create new message
                                final ParseObject message = new ParseObject("Message");
                                message.setACL(acl);
                                message.put("sender", sender);
                                message.put("recipient", recipient);
                                message.put("image", msgImage);
                                message.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            // Refresh list view with new message
                                            queryAndShowMessages();
                                        } else {
                                            Toast.makeText(getContext(), "Failed to send image", Toast.LENGTH_SHORT).show();
                                            Log.e(TAG, e.getMessage());
                                        }
                                        progressDialog.dismiss();
                                    }
                                });
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "Failed to send image", Toast.LENGTH_SHORT).show();
                                Log.e(TAG, e.getMessage());
                            }
                        }
                    }, new ProgressCallback() {
                        @Override
                        public void done(Integer percentDone) {
                            progressDialog.setProgress(percentDone);
                        }
                    });
                } catch (IOException e) {
                    // Failed to get image from intent
                    Toast.makeText(getContext(), R.string.failed_image_intent, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                }
            }
        }
    }

    private class MessagesListAdapter extends ArrayAdapter<ParseObject> {

        private static final String TAG = "MessagesListAdapter";

        private static final int resource = R.layout.message_item;

        public MessagesListAdapter(Context context, List<ParseObject> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                view = inflater.inflate(resource, parent, false);
            }
            final ImageView profilePicView = ButterKnife.findById(view, R.id.profile_pic);
            final ImageView msgImageView = ButterKnife.findById(view, R.id.message_image);
            TextView msgTextView = ButterKnife.findById(view, R.id.message_text);

            final ParseObject message = getItem(position);
            try {
                message.fetchIfNeeded();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Get message text
            String msgText = message.getString("text");
            if (msgText != null) {
                msgTextView.setText(msgText);
            } else {
                msgTextView.setText("");
            }

            // Get message image
            ParseFile msgImagePF = message.getParseFile("image");
            if (msgImagePF != null) {
                msgImagePF.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                            msgImageView.setImageBitmap(bitmap);
                        } else {
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
            } else {
                msgImageView.setImageResource(0);
            }

            // Get sender's profile pic
            ParseFile profilePicPF = message.getParseUser("sender").getParseFile("profilePic");
            if (profilePicPF != null) {
                profilePicPF.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                            profilePicView.setImageBitmap(bitmap);
                        } else {
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
            } else {
                profilePicView.setImageResource(R.drawable.blank_profile_pic);
            }

            if (message.getACL().getWriteAccess(ParseUser.getCurrentUser())) {
                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        // 1. Instantiate an AlertDialog.Builder with its constructor
                        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                                .setTitle("Delete Message?")
                                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        message.deleteInBackground(new DeleteCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if (e == null) {
                                                    // Refresh list with message removed
                                                    mMessages.remove(message);
                                                    listView.setAdapter(new MessagesListAdapter(getContext(), mMessages));
                                                } else {
                                                    Log.e(TAG, e.getMessage());
                                                }
                                            }
                                        });
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .create();
                        dialog.show();

                        return true;
                    }
                });
            }

            return view;
        }
    }

}
