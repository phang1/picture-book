package group14a.finalproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Patrick on 12/11/2015.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ViewHolder> {

    private static final String TAG = "RVAdapter";
    private static final int SELECT_IMAGE = 001;

    public List<ParseObject> albumsList;
    private String[] options = {"Delete", "Get Shared Users", "Share With a User", "Change Privacy"};
    private Context context;
    private AlbumSupport albumSupport;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;

        TextView title;
        ImageView privacyIcon;
        ImageView albumIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.album_name);
            privacyIcon = (ImageView) itemView.findViewById(R.id.privacy_icon);
            albumIcon = (ImageView) itemView.findViewById(R.id.album_image);
        }
    }

    public RVAdapter(ArrayList<ParseObject> albumsList, Context context, AlbumSupport albumSupport) {
        this.albumsList = albumsList;
        this.context = context;
        this.albumSupport = albumSupport;
        Log.d(TAG, "albumsList size " + albumsList.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.album_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ParseObject album = albumsList.get(position);
        final List<ParseFile> pictureFiles = album.getList("pictures");

        holder.title.setText(album.getString("name"));

        if (album.getBoolean("privacy")) {
            holder.privacyIcon.setImageResource(R.drawable.lock_icon);
        } else {
            holder.privacyIcon.setImageResource(R.drawable.unlock_icon);
        }

        if (pictureFiles == null || pictureFiles.isEmpty()) {
            holder.albumIcon.setImageResource(R.drawable.empty);
        } else {
            try {
                Bitmap imageBitmap = BitmapFactory.decodeByteArray(pictureFiles.get(0).getData(), 0, pictureFiles.get(0).getData().length);
                holder.albumIcon.setImageBitmap(imageBitmap);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment viewPhotosFragment = new Fragment() {
                    private List<ParseFile> mPictureFiles = new ArrayList<>();
                    private MyGridAdapter gridAdapter;

                    @Override
                    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                        View view = inflater.inflate(R.layout.fragment_view_pictures, container, false);

                        // Set grid view adapter
                        final GridView gridView = ButterKnife.findById(view, R.id.picture_grid);
                        gridAdapter = new MyGridAdapter(getActivity(), mPictureFiles, album);
                        gridView.setAdapter(gridAdapter);

                        getAndShowPictures();

                        // Set album name
                        TextView albumNameView = ButterKnife.findById(view, R.id.album_name);
                        albumNameView.setText(album.getString("name"));

                        // Set add picture button listener
                        Button addPictureButton = ButterKnife.findById(view, R.id.add_picture);
                        if (album.getList("invited") != null && album.getList("invited").contains(ParseUser.getCurrentUser().getObjectId().toString())) {
                            addPictureButton.setEnabled(true);
                        } else if (album.getParseUser("owner").getObjectId().equals(ParseUser.getCurrentUser().getObjectId())){
                            addPictureButton.setEnabled(true);
                        } else {
                            addPictureButton.setEnabled(false);
                        }
                        addPictureButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent grabPicture = new Intent(Intent.ACTION_PICK,
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(grabPicture, SELECT_IMAGE);
                            }
                        });

                        return view;
                    }

                    @Override
                    public void onResume() {
                        super.onResume();
                        getAndShowPictures();
                    }

                    @Override
                    public void onActivityResult(int requestCode, int resultCode, Intent data) {
                        if (resultCode == getActivity().RESULT_OK) {
                            if (requestCode == SELECT_IMAGE) {
                                final Uri imageUri = data.getData();

                                try {
                                    final ProgressDialog progressDialog = ProgressDialog.show(getContext()
                                            , "Loading...", "", true);

                                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),
                                            imageUri);
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                    ParseFile albumPicture = new ParseFile("pictures.png", stream.toByteArray());
                                    albumPicture.save();

                                    if (album.getList("invited") != null && album.getList("invited").contains(ParseUser.getCurrentUser().getObjectId().toString())) {
                                        album.add("pending", albumPicture);
                                        album.saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                progressDialog.dismiss();
                                                //NEED PUSH HERE
                                                //notify owner of album they got a picture request
                                                if (e == null) {
                                                    Toast.makeText(getContext(), "Picture addition requested", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(getContext(), "Error while requesting", Toast.LENGTH_SHORT).show();
                                                    Log.i(TAG, e.toString());
                                                }
                                            }
                                        });
                                    } else {
                                        album.add("pictures", albumPicture);
                                        album.saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                progressDialog.dismiss();
                                                if (e == null) {
                                                    Toast.makeText(getContext(), "Picture Saved", Toast.LENGTH_SHORT).show();
                                                    getAndShowPictures();
                                                } else {
                                                    Toast.makeText(getContext(), "Error while saving", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    }

                                } catch (Exception e) {

                                }

                            }
                        }
                    }

                    private void getAndShowPictures() {
                        album.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject album, ParseException e) {
                                List<ParseFile> pictures = album.getList("pictures");
                                mPictureFiles.clear();
                                if (pictures != null) {
                                    Log.i(TAG, "Retrieved " + pictures.size() + " pictures");
                                    mPictureFiles.addAll(pictures);
                                    Log.i(TAG, "mPictureFiles has " + mPictureFiles.size() + " pictures");
                                } else {
                                    Log.i(TAG, "No pictures found");
                                }
                                gridAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                };
                albumSupport.goToPhotos(viewPhotosFragment);
                //NEED TO IMPLEMENT GOING TO A VIEW ALBUM FRAGMENT
            }
        });

        //ONLY IF YOU OWN THE ALBUM
        if (album.getParseUser("owner") == ParseUser.getCurrentUser()) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    dialogBuilder.setTitle("Options");
                    dialogBuilder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                //should delete the object from the database
                                ParseObject deleteObject = albumsList.get(position);
                                ParseQuery<ParseObject> existingInvites = ParseQuery.getQuery("Album");
//                                existingInvites.whereContains("invited", )
                                albumsList.remove(position);
                                deleteObject.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            Toast.makeText(context, "Album was successfully deleted", Toast.LENGTH_SHORT);
                                        } else {
                                            Toast.makeText(context, "Error occurred while deleting", Toast.LENGTH_SHORT);
                                        }
                                    }
                                });
                            } else if (which == 1) {
                                //should get a list of shared users
                                List<String> invitedUserIds = album.getList("invited");
                                if (invitedUserIds == null) {
                                    return;
                                }
                                ParseQuery<ParseUser> usersQuery = ParseUser.getQuery();
                                usersQuery.whereContainedIn("objectId", invitedUserIds);
                                usersQuery.findInBackground(new FindCallback<ParseUser>() {
                                    @Override
                                    public void done(List<ParseUser> objects, ParseException e) {
                                        if (e == null) {
                                            String[] tempNames = new String[objects.size()];
                                            int i = 0;
                                            for (ParseUser tempUser : objects) {
                                                tempNames[i] = tempUser.getUsername().toString();
                                                Log.d(TAG, tempNames[i]);
                                                ++i;
                                            }

                                            //ERROR HERE
                                            AlertDialog.Builder listOfShared_builder = new AlertDialog.Builder(context);
                                            listOfShared_builder.setTitle("Shared Users");
                                            listOfShared_builder.setItems(tempNames, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // should be empty
                                                }
                                            });
                                            listOfShared_builder.create().show();

                                        } else
                                            Log.e(TAG, e.toString());
                                    }
                                });

                            } else if (which == 2) {
                                // Show list of users
                                Intent intent = new Intent(context, UsersListActivity.class);
                                intent.putExtra("albumId", album.getObjectId());
                                ((Activity) context).startActivityForResult(intent, UsersListActivity.PICK_USER);

//                                //SHOULD ALLOW THE OWNER TO SHARE THE ALBUM WITH A USER
//                                //NEED A PUSH HERE
//                                //NEED TO NOTIFY THE INVITED USER THEY WERE INVITED
//
//                                LayoutInflater inflater = LayoutInflater.from(context);
//                                View promptUser = inflater.inflate(R.layout.promp_invite, null);
//
//                                final AlertDialog.Builder inviteBuilder =
//                                        new AlertDialog.Builder(context);
//                                inviteBuilder.setView(promptUser);
//
//                                final EditText userName = (EditText) promptUser.findViewById(R.id.inviteUser);
//
//                                inviteBuilder
//                                        .setCancelable(false)
//                                        .setPositiveButton("Invite",
//                                                new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        if (userName.getText() != null) {
//                                                            ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
//                                                            userQuery.whereEqualTo("username", userName.getText().toString());
//
//                                                            userQuery.findInBackground(new FindCallback<ParseUser>() {
//                                                                @Override
//                                                                public void done(List<ParseUser> objects, ParseException e) {
//                                                                    if (e == null) {
//
//                                                                        album.add("invited", objects.get(0).getObjectId());
//
//                                                                        try {
//                                                                            album.save();
//                                                                        } catch (ParseException e1) {
//                                                                            e1.printStackTrace();
//                                                                        }
//
//                                                                    }
//                                                                }
//                                                            });
//                                                        }
//
//                                                    }
//                                                })
//                                        .setNegativeButton("Cancel",
//                                                new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        dialog.cancel();
//                                                    }
//                                                })
//                                        .show();
                            } else {
                                //SHOULD ALLOW THE OWNER TO CHANGE THE PRIVACY OF THE ALBUM

                                album.put("privacy", !album.getBoolean("privacy"));
                                album.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {

                                        if (e == null)
                                            if (album.getBoolean("privacy"))
                                                Toast.makeText(context, album.getString("name") +
                                                        " has been changed to a private album", Toast.LENGTH_SHORT).show();
                                            else
                                                Toast.makeText(context, album.getString("name") +
                                                        " has been changed to a public album", Toast.LENGTH_SHORT).show();

                                    }
                                });
                            }
                        }
                    });

                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    return false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }

    public interface AlbumSupport {
        void goToPhotos(Fragment viewPhotos);
    }
}
