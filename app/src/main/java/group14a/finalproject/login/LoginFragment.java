package group14a.finalproject.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.Profile;
import com.facebook.login.widget.LoginButton;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import butterknife.Bind;
import butterknife.ButterKnife;
import group14a.finalproject.R;


/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";

    private boolean isAuthTaskRunning = false;

    @Bind(R.id.edit_username) AutoCompleteTextView usernameView;
    @Bind(R.id.edit_password) EditText passwordView;
    @Bind(R.id.login_button) Button loginButton;
    @Bind(R.id.create_account_button) Button signupButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        /* Setup Facebook login button */
        LoginButton facebookButton = ButterKnife.findById(view, R.id.facebook_button);
        facebookButton.setFragment(this);
        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginFragment.this, null, new LogInCallback() {
                    @Override
                    public void done(final ParseUser user, ParseException err) {
                        if (user == null) {
                            Log.d(TAG, "Uh oh. The user cancelled the Facebook login.");
                        } else if (user.isNew()) {
                            Log.d(TAG, "User signed up and logged in through Facebook!");

                            /* Link Facebook and Parse accounts */
                            if (!ParseFacebookUtils.isLinked(user)) {
                                ParseFacebookUtils.linkWithReadPermissionsInBackground(user, LoginFragment.this, null, new SaveCallback() {
                                    @Override
                                    public void done(ParseException ex) {
                                        if (ParseFacebookUtils.isLinked(user)) {
                                            Log.d(TAG, "Woohoo, user logged in with Facebook!");
                                        }
                                    }
                                });
                            }

                            Profile profile = Profile.getCurrentProfile();
                            user.put("firstName", profile.getFirstName());
                            user.put("lastName", profile.getLastName());
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e != null) {
                                        Log.e(TAG, "Failed to save Facebook first and last name to Parse");
                                        Log.e(TAG, e.getMessage());
                                    }
                                }
                            });
                            LoginActivity.postLogin(getActivity());
                        } else {
                            Log.d(TAG, "User logged in through Facebook!");
                            LoginActivity.postLogin(getActivity());
                        }
                    }
                });
            }
        });

        /* Setup Twitter login button */
        Button twitterButton = ButterKnife.findById(view, R.id.twitter_button);
//        twitterButton.setClickable(true);
        twitterButton.setEnabled(true);
        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "it should be going to parse");
                ParseTwitterUtils.logIn(getContext(), new LogInCallback() {
                    @Override
                    public void done(final ParseUser user, ParseException err) {
                        if (user == null) {
                            Log.d(TAG, "Uh oh. The user cancelled the Twitter login.");
                        } else if (user.isNew()) {
                            Log.d(TAG, "User signed up and logged in through Twitter!");

                            /* Link Twitter and Parse accounts */
                            if (!ParseTwitterUtils.isLinked(user)) {
                                ParseTwitterUtils.link(user, getContext(), new SaveCallback() {
                                    @Override
                                    public void done(ParseException ex) {
                                        if (ParseTwitterUtils.isLinked(user)) {
                                            Log.d(TAG, "Woohoo, user logged in with Twitter!");
                                        }
                                    }
                                });
                            }

                            LoginActivity.postLogin(getActivity());
                        } else {
                            Log.d(TAG, "User logged in through Twitter!");
                            LoginActivity.postLogin(getActivity());
                        }
                    }
                });
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "signin button clicked from login");
                attemptLogin();
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "signup button clicked from login");
                Log.d(TAG, "login --> signup");
                // --> Signup
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new SignupFragment(), "SignupFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (isAuthTaskRunning) {
            return;
        }

        // Reset errors
        usernameView.setError(null);
        passwordView.setError(null);

        final String email = usernameView.getText().toString();
        final String password = passwordView.getText().toString();

        // Validate fields are non-empty
        boolean validationError = false;
        View focusView = null;
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            focusView = passwordView;
            validationError = true;
        }
        if (TextUtils.isEmpty(email)) {
            usernameView.setError(getString(R.string.error_field_required));
            focusView = usernameView;
            validationError = true;
        }
        if (validationError) {
            focusView.requestFocus();
            return;
        }

        // Attempt Login
        isAuthTaskRunning = true;
        showProgress(true);
        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.logOut();
        Log.i(TAG, "attemping Parse login");
        ParseUser.logInInBackground(email, password, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                isAuthTaskRunning = false;
                showProgress(false);
                if (e == null && user != null) {
                    // User is logged in
                    Log.i(TAG, "login succeeded");
                    passwordView.setText("");
                    LoginActivity.postLogin(getActivity());
                } else if (e == null && user == null) {
                    Log.w(TAG, "Parse log in did not succeed");
                    Log.w(TAG, "e == null && user == null");
                    passwordView.setError(getString(R.string.error_incorrect_password));
                    passwordView.requestFocus();
                } else {
                    Log.e(TAG, "Parse login did not succeed");
                    Log.w(TAG, "e != null && user == null");
                    Log.e(TAG, e.getMessage());
                    Toast.makeText(getContext(), R.string.failed_login, Toast.LENGTH_SHORT).show();
                    loginButton.requestFocus();
                }
            }
        });
    }

    @Bind(R.id.login_form) View formView;
    @Bind(R.id.login_progress) View progressView;

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult requestCode=" + requestCode + " resultCode=" + resultCode);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }
}
