package group14a.finalproject.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import group14a.finalproject.R;


/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class SignupFragment extends Fragment {

    private static final String TAG = "SignupFragment";

    private boolean isAuthTaskRunning = false;

    @Bind(R.id.edit_first_name) EditText firstNameView;
    @Bind(R.id.edit_last_name) EditText lastNameView;
    @Bind(R.id.edit_username) EditText usernameView;
    @Bind(R.id.edit_password1) EditText password1View;
    @Bind(R.id.edit_password2) EditText password2View;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        ButterKnife.bind(this, view);

        Button signupButton = ButterKnife.findById(view, R.id.signup_button);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "signup button clicked");
                attemptSignup();
            }
        });

        Button cancelButton = ButterKnife.findById(view, R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .popBackStack();
                onDetach();
            }
        });

        return view;
    }

    private void attemptSignup() {
        if (isAuthTaskRunning) {
            return;
        }

        // Reset errors
        firstNameView.setError(null);
        lastNameView.setError(null);
        usernameView.setError(null);
        password1View.setError(null);
        password2View.setError(null);

        // Store values at the time of the login attempt.
        final String firstName = firstNameView.getText().toString();
        final String lastName = lastNameView.getText().toString();
        final String username = usernameView.getText().toString();
        final String password1 = password1View.getText().toString();
        final String password2 = password2View.getText().toString();

        boolean validationError = false;
        View focusView = null;

        // Validate passwords are non-empty and match
        if (TextUtils.isEmpty(password2)) {
            password2View.setError(getString(R.string.error_field_required));
            focusView = password2View;
            validationError = true;
        }
        if (TextUtils.isEmpty(password1)) {
            password1View.setError(getString(R.string.error_field_required));
            focusView = password1View;
            validationError = true;
        } else if (!password1.equals(password2)) {
            password1View.setError(getString(R.string.error_password_mismatch));
            focusView = password1View;
            validationError = true;
        }

        // Check for non-empty username
        if (TextUtils.isEmpty(username)) {
            usernameView.setError(getString(R.string.error_field_required));
            focusView = usernameView;
            validationError = true;
        }

        // Check for non-empty last name
        if (TextUtils.isEmpty(lastName)) {
            lastNameView.setError(getString(R.string.error_field_required));
            focusView = lastNameView;
            validationError = true;
        }

        // Check for non-empty first name
        if (TextUtils.isEmpty(firstName)) {
            firstNameView.setError(getString(R.string.error_field_required));
            focusView = firstNameView;
            validationError = true;
        }

        if (validationError) {
            focusView.requestFocus();
            return;
        }

        // Attempt sign up
        isAuthTaskRunning = true;
        showProgress(true);
        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.logOut();
        final ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password1);
        user.put("firstName", firstName);
        user.put("lastName", lastName);
        Log.i(TAG, "attempting Parse sign up");
        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                isAuthTaskRunning = false;
                showProgress(false);
                if (e == null) {
                    // User is signed up
                    Log.i(TAG, "signup succeded");
                    password1View.setText("");
                    password2View.setText("");
                    LoginActivity.postLogin(getActivity());
                } else {
                    // Sign up didn't succeed. Look at the ParseException
                    // to figure out what went wrong
                    Log.e(TAG, "Parse signup did not succeed");
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }
        });
    }

    @Bind(R.id.signup_form) View formView;
    @Bind(R.id.signup_progress) View progressView;

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}
