package group14a.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.ButterKnife;
import group14a.finalproject.login.LoginActivity;
import group14a.finalproject.messages.ConvosListFragment;
import group14a.finalproject.profile.ProfileFragment;

/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class MainActivity extends AppCompatActivity implements
        ListAlbumFragment.OnFragmentInteractionListener,
        RVAdapter.AlbumSupport {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If a user isn't logged in, go to LoginActivity
        ParseUser user = ParseUser.getCurrentUser();
        if (user == null || !user.isAuthenticated()) {
            logoutAndGoToLogin();
            return;
        }
        // A user should now be logged in

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Log.d(TAG, "main onCreate --> profile fragment");
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new ProfileFragment())
//                .addToBackStack(null)
                .commit();
    }

    private void logoutAndGoToLogin() {
        ParseUser.logOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToCreateAlbum() {
        Log.i(TAG, "goToCreateAlbum");
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new CreateAlbumFragment(), "CreateAlbumFragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.goto_profile:
                Log.d(TAG, "main menu --> profile");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, ProfileFragment.newInstance())
                        .commit();
                return true;
            case R.id.goto_albums:
                Log.i(TAG, "clicked menu albums");
                goToList();
                return true;
            case R.id.goto_convos:
                Log.d(TAG, "main menu --> convos");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new ConvosListFragment())
                        .commit();
                return true;
            case R.id.goto_logout:
                Log.d(TAG, "main menu --> logout --> login");
                logoutAndGoToLogin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "on activity result");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == UsersListActivity.PICK_USER) {
                Log.i(TAG, "picked user");
                String userId = data.getStringExtra("userId");
                String albumId = data.getStringExtra("albumId");

                ParseObject album = ParseObject.createWithoutData("Album", albumId);
                album.add("invited", userId);
                album.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.i(TAG, "invited saved!!");
                        } else {
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
            }
        }
    }

    public void goToList() {
        Log.i(TAG, "goToList");
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new ListAlbumFragment(), "ListAlbumFragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void goToPhotos(Fragment viewPhotos) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, viewPhotos, "ViewPicturesFragment")
                .addToBackStack(null)
                .commit();
    }
}
