package group14a.finalproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Patrick on 12/13/2015.
 */
public class MyGridAdapter extends BaseAdapter {
    private FragmentActivity mContext;
    private List<ParseFile> pictures;
    private static final String TAG = "MyGridAdapter";
    private ParseObject album;
    public MyGridAdapter(FragmentActivity context, List<ParseFile> pictures, ParseObject album) {
        mContext = context;
        this.pictures = pictures;
        this.album = album;
    }

    @Override
    public int getCount() {
        return pictures.size();
    }

    @Override
    public Object getItem(int position) {
        return pictures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, final View convertView, final ViewGroup parent) {
        Log.i(TAG, "getView " + position);

        final ImageView imageView;
        final Bitmap imageBitmap;
        final int inPosition = position;

        if (convertView == null) {
            imageView = new ImageView(parent.getContext());
            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        } else {
            imageView = (ImageView) convertView;
        }


        Log.i(TAG, "pictures has " + pictures.size() + " pictures");

        try {
            ParseFile picture = pictures.get(position);
            imageBitmap = BitmapFactory.decodeByteArray(picture.getData(), 0, picture.getData().length);
            imageView.setImageBitmap(imageBitmap);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Fragment individPicFrag = new Fragment() {

                    @Override
                    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
                        super.onCreateView(inflater, parent, savedInstanceState);
                        FrameLayout tempLayout = new FrameLayout(getContext());
                        ImageView individPicture = new ImageView(getContext());

                        if (album.getParseUser("owner").getObjectId().equals(ParseUser.getCurrentUser().getObjectId()))
                            individPicture.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {
                                    final AlertDialog.Builder deletePhoto = new AlertDialog.Builder(getContext());
                                    deletePhoto.setTitle("Delete?");
                                    deletePhoto.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            List<ParseFile> deleteMe = Arrays.asList(pictures.get(position));
                                            album.removeAll("pictures", deleteMe);
                                            try {
                                                album.save();
                                                getActivity().getSupportFragmentManager()
                                                        .popBackStack();
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                    return true;
                                }
                            });

                        try {
                            Bitmap picBitmap = BitmapFactory.decodeByteArray(pictures.get(inPosition).getData(), 0, pictures.get(inPosition).getData().length);
                            individPicture.setImageBitmap(picBitmap);
                        } catch (ParseException e) {
                            individPicture.setImageResource(R.drawable.empty);
                            e.printStackTrace();
                        }

                        tempLayout.addView(individPicture);

                        return tempLayout;
                    }
                };

                mContext.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, individPicFrag, "PictureFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });
        
        return imageView;
    }

}

