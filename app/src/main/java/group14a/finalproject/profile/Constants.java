package group14a.finalproject.profile;

/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class Constants {

    // Gender values for Parse
    static final String GENDER_MALE = "M";
    static final String GENDER_FEMALE = "F";
    static final String GENDER_OTHER = "O";

}
