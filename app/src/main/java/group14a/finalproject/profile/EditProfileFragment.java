package group14a.finalproject.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import group14a.finalproject.R;


/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class EditProfileFragment extends Fragment {

    private static final String TAG = "EditProfileFragment";

    @Bind(R.id.edit_username) EditText usernameView;
    @Bind(R.id.edit_first_name) EditText firstNameView;
    @Bind(R.id.edit_last_name) EditText lastNameView;
    @Bind(R.id.group_gender) RadioGroup genderView;
    @Bind(R.id.gender_male) RadioButton genderMaleRButton;
    @Bind(R.id.gender_female) RadioButton genderFemaleRButton;
    @Bind(R.id.gender_other) RadioButton genderOtherRButton;
    @Bind(R.id.save_button) Button saveButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, view);

        getActivity().setTitle(R.string.edit_profile);

        Log.d(TAG, "onCreateView");

        final ParseUser user = ParseUser.getCurrentUser();

        // Set username, first name, last name
        usernameView.setText(user.getUsername());
        firstNameView.setText(user.getString("firstName"));
        lastNameView.setText(user.getString("lastName"));

        // Set gender
        String gender = user.getString("gender");
        if (gender != null) {
            switch (gender) {
                case Constants.GENDER_MALE:
                    genderMaleRButton.setChecked(true);
                    break;
                case Constants.GENDER_FEMALE:
                    genderFemaleRButton.setChecked(true);
                    break;
                case Constants.GENDER_OTHER:
                    genderOtherRButton.setChecked(true);
                    break;
            }
        }

        // Set profile private switch
        final SwitchCompat profilePrivateSwitch = ButterKnife.findById(view, R.id.profile_private_switch);
        profilePrivateSwitch.setChecked(user.getBoolean("isPrivate"));

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "clicked save button");

                user.setUsername(usernameView.getText().toString());
                user.put("firstName", firstNameView.getText().toString());
                user.put("lastName", lastNameView.getText().toString());
                switch (genderView.getCheckedRadioButtonId()) {
                    case R.id.gender_male:
                        user.put("gender", Constants.GENDER_MALE);
                        break;
                    case R.id.gender_female:
                        user.put("gender", Constants.GENDER_FEMALE);
                        break;
                    case R.id.gender_other:
                        user.put("gender", Constants.GENDER_OTHER);
                        break;
                }
                user.put("isPrivate", profilePrivateSwitch.isChecked());

                Log.d(TAG, "saving user profile");
                user.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.d(TAG, "user profile saved");
                            Toast.makeText(getContext(), R.string.profile_saved, Toast.LENGTH_SHORT).show();

                            // Go back
                            getActivity().getSupportFragmentManager()
                                    .popBackStack();
                            onDestroy();
                        } else {
                            Log.e(TAG, e.getMessage());
                            Toast.makeText(getContext(), R.string.failed_profile_save, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        return view;
    }

}
