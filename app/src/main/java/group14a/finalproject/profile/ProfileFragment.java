package group14a.finalproject.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import group14a.finalproject.R;
import group14a.finalproject.UsersListActivity;


/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class ProfileFragment extends Fragment {

    private static final String TAG = "ProfileFragment";

    // Intent request code
    private static final int SELECT_IMAGE = 1001;

    private String userId;

    public static ProfileFragment newInstance(String userId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString("userId", userId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString("userId");
        } else {
            userId = ParseUser.getCurrentUser().getObjectId();
        }
        setHasOptionsMenu(true);
    }

    @Bind(R.id.profile_pic) ImageView profilePicView;
    @Bind(R.id.username) TextView usernameView;
    @Bind(R.id.name) TextView nameView;
    @Bind(R.id.group_gender) TextView genderView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        getActivity().setTitle(R.string.profile);

        Log.d(TAG, "onCreateView");

        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        userQuery.getInBackground(userId, new GetCallback<ParseUser>() {
            @Override
            public void done(final ParseUser user, ParseException e) {
                // Download profile picture
                ParseFile profilePicPF = user.getParseFile("profilePic");
                if (profilePicPF != null) {
                    profilePicPF.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {
                            if (e == null) {
                                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                profilePicView.setImageBitmap(bitmap);
                            } else {
                                Log.e(TAG, e.getMessage());
                                Log.e(TAG, "" + e.getCode());
                                Toast.makeText(getContext(), R.string.failed_profile_pic_download, Toast.LENGTH_SHORT).show();
                                profilePicView.setImageResource(R.drawable.blank_profile_pic);
                            }
                        }
                    });
                } else {
                    profilePicView.setImageResource(R.drawable.blank_profile_pic);
                }

                // Set username and first and last name
                String username = user.getUsername();
                String firstName = user.getString("firstName");
                String lastName = user.getString("lastName");
                StringBuilder fullNameBuilder = new StringBuilder();
                if (firstName != null)
                    fullNameBuilder.append(firstName);
                if (firstName != null && lastName != null)
                    fullNameBuilder.append(" ");
                if (lastName != null)
                    fullNameBuilder.append(lastName);
                usernameView.setText(username);
                nameView.setText(fullNameBuilder.toString());

                // Set gender
                Log.i(TAG, user.getUsername());
                String gender = user.getString("gender");
                if (gender != null) {
                    switch (gender) {
                        case Constants.GENDER_MALE:
                            genderView.setText(R.string.gender_male);
                            break;
                        case Constants.GENDER_FEMALE:
                            genderView.setText(R.string.gender_female);
                            break;
                        case Constants.GENDER_OTHER:
                            genderView.setText(R.string.gender_other);
                            break;
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_profile, menu);

        // Remove edit profile options if not logged in user
        if (userId != null && !ParseUser.getCurrentUser().getObjectId().equals(userId)) {
            menu.findItem(R.id.goto_edit_profile).setVisible(false);
            menu.findItem(R.id.goto_edit_profile_picture).setVisible(false);
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.goto_edit_profile:
                Log.d(TAG, "profile menu --> edit profile");
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new EditProfileFragment())
                        .addToBackStack(null)
                        .commit();
                return true;
            case R.id.goto_edit_profile_picture:
                Log.d(TAG, "profile menu --> edit profile picture");
                intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, SELECT_IMAGE);
                return true;
            case R.id.goto_users_list:
                Log.d(TAG, "profile menu --> users list");
                intent = new Intent(getContext(), UsersListActivity.class);
                startActivityForResult(intent, UsersListActivity.PICK_USER);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE) {
                final Uri imageUri = data.getData();
                try {
                    final ProgressDialog progressDialog = ProgressDialog.show(getContext(),
                            getString(R.string.loading), "", true);

                    // URI -> bitmap -> PNG byte[] -> ParseFile
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(
                            getContext().getContentResolver(), imageUri);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    final ParseFile profilePicPF = new ParseFile("profilePic.png", stream.toByteArray());

                    // Upload image to Parse
                    progressDialog.setIndeterminate(false);
                    progressDialog.setMax(100);
                    profilePicPF.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                ParseUser user = ParseUser.getCurrentUser();
                                user.put("profilePic", profilePicPF);
                                user.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        progressDialog.dismiss();
                                        if (e == null) {
                                            profilePicView.setBackgroundDrawable(null); //TODO needed?
                                            profilePicView.setImageURI(imageUri);
                                            Log.d(TAG, "new profile pic set");
                                        } else {
                                            Toast.makeText(getContext(), R.string.failed_profile_pic_upload, Toast.LENGTH_SHORT).show();
                                            Log.e(TAG, e.getMessage());
                                        }
                                    }
                                });
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), R.string.failed_profile_pic_upload, Toast.LENGTH_SHORT).show();
                                Log.e(TAG, e.getMessage());
                            }
                        }
                    }, new ProgressCallback() {
                        @Override
                        public void done(Integer percentDone) {
                            progressDialog.setProgress(percentDone);
                        }
                    });
                } catch (IOException e) {
                    // Failed to get image from intent
                    Toast.makeText(getContext(), R.string.failed_image_intent, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                }
            } else if (requestCode == UsersListActivity.PICK_USER) {
                String userId = data.getStringExtra("userId");
                Log.i(TAG, "picked user " + userId);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, ProfileFragment.newInstance(userId))
                        .addToBackStack(null)
                        .commit();
            }
        }
    }

}
