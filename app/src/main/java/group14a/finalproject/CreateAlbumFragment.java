package group14a.finalproject;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CreateAlbumFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CreateAlbumFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateAlbumFragment extends Fragment {

    private static final String TAG = "CreateFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateAlbumFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateAlbumFragment newInstance(String param1, String param2) {
        CreateAlbumFragment fragment = new CreateAlbumFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CreateAlbumFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    //----------------------------------------------------------------------------------------------

    @Bind(R.id.submit_album) Button createBtn;
    @Bind(R.id.album_cancel_button) Button cancelBtn;
    @Bind(R.id.album_name) EditText nameTxt;
    @Bind(R.id.privacy_settings) Switch privacySwitch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_album, container, false);
        ButterKnife.bind(this, view);



        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String albumName = nameTxt.getText().toString();
                if (albumName.isEmpty()) {
                    Toast.makeText(getContext(), "Invalid album name", Toast.LENGTH_SHORT).show();
                    return;
                }

                ParseUser user = ParseUser.getCurrentUser();
                user.add("owned", nameTxt.getText().toString());

                ParseObject album = new ParseObject("Album");
                album.put("name", albumName);
                album.put("privacy", privacySwitch.isChecked());

                ParseACL albumSecurity = new ParseACL();

                //sets the ACL settings depending on the privacy settings selected
                if(privacySwitch.isChecked()){
                    albumSecurity.setPublicWriteAccess(true);
                    albumSecurity.setWriteAccess(ParseUser.getCurrentUser(), true);
                    albumSecurity.setPublicReadAccess(true);
                    album.setACL(albumSecurity);
                }
                else{
                    albumSecurity.setPublicReadAccess(true);
                    albumSecurity.setPublicWriteAccess(true);
                    albumSecurity.setWriteAccess(ParseUser.getCurrentUser(), true);
                    album.setACL(albumSecurity);
                }

                album.put("owner", user);

                user.saveInBackground();
                album.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Toast.makeText(getActivity(), "Album Created and Saved", Toast.LENGTH_SHORT).show();
                            getFragmentManager().popBackStack();
                        } else {
                            Toast.makeText(getActivity(), "Error while creating the album", Toast.LENGTH_SHORT).show();
                            Log.i(TAG, e.toString());
                        }
                    }
                });

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
