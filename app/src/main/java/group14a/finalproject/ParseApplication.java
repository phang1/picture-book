package group14a.finalproject;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseTwitterUtils;


/**
 * Group 14A - Patrick Hang, Anish Patel, Divya Patel
 * Final Project
 */
public class ParseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.enableLocalDatastore(this);
        Parse.initialize(this);
        ParseInstallation.getCurrentInstallation();

        ParseACL defaultACL = new ParseACL();
        ParseACL.setDefaultACL(defaultACL, true);

        ParseFacebookUtils.initialize(getApplicationContext());

        ParseTwitterUtils.initialize("qsDtfVvvym5dbfzN53DNyQo0d", "rTE5ql6PVnMhXy0jMKIa8LyK0FexKG9en4t4ldtlGSHLcJNa8O");
    }
}
