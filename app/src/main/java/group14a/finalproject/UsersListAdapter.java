package group14a.finalproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Anish Patel
 * Final Exam
 */
public class UsersListAdapter extends ArrayAdapter<ParseUser> {

    private static final String TAG = "UsersListAdapter";

    private static final int resource = android.R.layout.activity_list_item;

    public UsersListAdapter(Context context, List<ParseUser> users) {
        super(context, resource, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(resource, parent, false);
        }
        final ImageView profilePicView = ButterKnife.findById(view, android.R.id.icon);
        TextView nameView = ButterKnife.findById(view, android.R.id.text1);

        ParseUser user = getItem(position);
        try {
            user.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String name = user.getString("firstName") + " " + user.getString("lastName");
        nameView.setText(name);

        // Download user's profile picture
        ParseFile profilePicPF = user.getParseFile("profilePic");
        if (profilePicPF != null) {
            profilePicPF.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                        profilePicView.setImageBitmap(bitmap);
                    } else {
                        Log.e(TAG, e.getMessage());
                    }
                }
            });
        } else {
            profilePicView.setImageResource(R.drawable.blank_profile_pic);
        }

        return view;
    }
}
