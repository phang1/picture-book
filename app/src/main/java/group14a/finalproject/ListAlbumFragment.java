package group14a.finalproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListAlbumFragment extends Fragment {

    private static final String TAG = "ListAlbumFragment";

    PagerAdapter pagerAdapter;
    private OnFragmentInteractionListener mListener;
    RVAdapter.AlbumSupport albumSupport;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            albumSupport = (RVAdapter.AlbumSupport) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Bind(R.id.createAlbum) Button goToCreateBtn;
    @Bind(R.id.tabView) ViewPager viewPager;
    @Bind(R.id.viewPending) Button goToPendingBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_list_album, container, false);
        ButterKnife.bind(this, view);

        getActivity().setTitle("Albums");

        pagerAdapter = new PagerAdapter(getChildFragmentManager(), albumSupport);
        viewPager.setAdapter(pagerAdapter);

        goToCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "attempting to create an album");
                mListener.goToCreateAlbum();
            }
        });

        goToPendingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment pendingFragment = new PendingListFragment();

                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, pendingFragment, "PendingFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        pagerAdapter = new PagerAdapter(getChildFragmentManager(), albumSupport);
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        albumSupport = null;
    }

    public interface OnFragmentInteractionListener {
        void goToCreateAlbum();
    }

    public static class PagerAdapter extends FragmentStatePagerAdapter {

        private RVAdapter.AlbumSupport albumSupport;

        public PagerAdapter(FragmentManager fragmentManager, RVAdapter.AlbumSupport albumSupport) {
            super(fragmentManager);
            this.albumSupport = albumSupport;
        }

        @Override
        public int getCount() {
            return 3;
        }

        /** Returns a fragment to be used in the ViewPager tabView */
        @Override
        public Fragment getItem(final int position) {
            return new Fragment() {

                private ArrayList<ParseObject> albumsList = new ArrayList<>();
                private RVAdapter rvAdapter;

                @Override
                public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                    View view = inflater.inflate(R.layout.fragment_adapter, container, false);

                    TextView fragmentTitleView = ButterKnife.findById(view, R.id.fragment_title);
                    final RecyclerView mRecyclerView = ButterKnife.findById(view, R.id.recycler_view);

                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                    rvAdapter = new RVAdapter(albumsList, getContext(), albumSupport);
                    mRecyclerView.setAdapter(rvAdapter);

                    ParseQuery<ParseObject> albumsQuery = ParseQuery.getQuery("Album");
                    switch (position) {
                        case 0:
                            Log.i(TAG, "loading owned albums");
                            fragmentTitleView.setText("Owned Albums");
                            albumsQuery.whereEqualTo("owner", ParseUser.getCurrentUser());
                            break;
                        case 1:
                            Log.i(TAG, "loading invited albums");
                            fragmentTitleView.setText("Invited Albums");
                            albumsQuery.whereEqualTo("invited", ParseUser.getCurrentUser().getObjectId());
                            break;
                        case 2:
                            Log.i(TAG, "loading public albums");
                            fragmentTitleView.setText("Public Albums");
                            albumsQuery.whereEqualTo("privacy", false);
                            break;
                    }

                    final ProgressDialog progressDialog = ProgressDialog.show(getContext(), "Loading...", "");
                    albumsQuery.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {
                            if (e == null) {
                                albumsList.clear();
                                albumsList.addAll(objects);
                                rvAdapter.notifyDataSetChanged();
                            } else {
                                Log.e(TAG, e.getMessage());
                            }
                            progressDialog.dismiss();
                        }
                    });

                    return view;
                }
            };
        }
    }

    public static class PendingListFragment extends ListFragment {
        final List<ParseFile> pendingPics = new ArrayList<>();
        final List<String> albumNames = new ArrayList<>();

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            getActivity().setTitle("Pending Pictures");

            final ParseQuery<ParseObject> albumsQuery = ParseQuery.getQuery("Album");
            albumsQuery.whereEqualTo("owner", ParseUser.getCurrentUser());
            albumsQuery.whereExists("pending");
            final ProgressDialog progressDialog = ProgressDialog.show(getContext(), "Loading...", "");
            albumsQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> pendingAlbums, ParseException e) {
                    if (e == null) {
                        for (ParseObject pendingAlbum : pendingAlbums) {
                            List<ParseFile> albumPendingPics = pendingAlbum.getList("pending");
                            for (ParseFile pendingPic : albumPendingPics) {
                                pendingPics.add(pendingPic);
                                albumNames.add(pendingAlbum.getString("name"));
                            }
                        }

                        final ArrayAdapter<ParseFile> pendingAdapter = new ArrayAdapter<ParseFile>(getContext(), R.layout.pending_pic_item, pendingPics) {
                            @Override
                            public View getView(final int position, View convertView, ViewGroup parent) {
                                View view = convertView;
                                if (view == null) {
                                    LayoutInflater inflater = LayoutInflater.from(getContext());
                                    view = inflater.inflate(R.layout.pending_pic_item, parent, false);
                                }
                                TextView albumTitleView = ButterKnife.findById(view, R.id.text1);
                                final ImageView pendingPicView = ButterKnife.findById(view, R.id.icon);

                                final String albumName = albumNames.get(position);
                                albumTitleView.setText(albumName);

                                final ParseFile pendingPic = getItem(position);
                                pendingPic.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                            pendingPicView.setImageBitmap(bitmap);
                                        } else {
                                            Log.e(TAG, e.getMessage());
                                        }
                                    }
                                });

                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        new AlertDialog.Builder(getContext())
                                                .setTitle("Pending Photo")
                                                .setPositiveButton("Approve", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        ParseQuery<ParseObject> albumsQuery = new ParseQuery<>("Album");
                                                        albumsQuery.whereEqualTo("name", albumName);
                                                        albumsQuery.findInBackground(new FindCallback<ParseObject>() {
                                                            @Override
                                                            public void done(List<ParseObject> objects, ParseException e) {
                                                                final ParseObject album = objects.get(0);
                                                                album.add("pictures", pendingPic);
                                                                album.saveInBackground(new SaveCallback() {
                                                                    @Override
                                                                    public void done(ParseException e) {
                                                                        if (e == null) {
                                                                            //NEED PUSH HERE OF THE ONE WHO REQUESTED PICTURE
                                                                            album.removeAll("pending", Arrays.asList(pendingPic));
                                                                            album.saveInBackground(new SaveCallback() {
                                                                                @Override
                                                                                public void done(ParseException e) {
                                                                                    if (e == null) {

                                                                                    } else {
                                                                                        Log.e(TAG, e.getMessage());
                                                                                    }
                                                                                }
                                                                            });
                                                                        } else {
                                                                            Log.e(TAG, e.getMessage());
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                })
                                                .setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        ParseQuery<ParseObject> albumsQuery = new ParseQuery<>("Album");
                                                        albumsQuery.whereEqualTo("name", albumName);
                                                        final List<ParseFile> removeMe = Arrays.asList(pendingPics.get(position));
                                                        albumsQuery.findInBackground(new FindCallback<ParseObject>() {
                                                            @Override
                                                            public void done(List<ParseObject> objects, ParseException e) {
                                                                objects.get(0).removeAll("pending", removeMe);
                                                                try {
                                                                    objects.get(0).save();
                                                                } catch (ParseException e1) {
                                                                    e1.printStackTrace();
                                                                }
                                                            }
                                                        });
                                                    }
                                                })
                                                .create()
                                                .show();
                                    }
                                });
                                return view;
                            }
                        };
                        setListAdapter(pendingAdapter);
                        setEmptyText("No pending pictures found");

                        progressDialog.dismiss();
                    } else
                        Log.i(TAG, e.toString());
                }
            });
        }
    }

}
