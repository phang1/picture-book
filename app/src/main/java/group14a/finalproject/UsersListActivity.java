package group14a.finalproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class UsersListActivity extends AppCompatActivity {

    private static final String TAG = "UsersListActivity";

    // Intent request code
    public static final int PICK_USER = 900;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);

        setTitle("Select a User");

        final TextView emptyListView = ButterKnife.findById(this, R.id.empty_list);
        final ListView listView = ButterKnife.findById(this, R.id.list);

        final ArrayList<ParseUser> mUsers = new ArrayList<>();
        final UsersListAdapter adapter = new UsersListAdapter(this, mUsers);
        listView.setAdapter(adapter);
        listView.setEmptyView(emptyListView);

        ParseQuery<ParseUser> usersQuery = ParseUser.getQuery();
        usersQuery.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
        usersQuery.whereEqualTo("isPrivate", false);
        usersQuery.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> users, ParseException e) {
                if (e == null) {
                    mUsers.addAll(users);
                    adapter.notifyDataSetChanged();
                } else {
                    Log.e(TAG, e.getMessage());
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseUser user = mUsers.get(position);
                String userId = user.getObjectId();
                String userName = user.getString("firstName") + " " + user.getString("lastName");
                Intent data = getIntent();
                data.putExtra("userId", userId);
                data.putExtra("userName", userName);
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }
}
